# Spring Boot JWT Auth

Run this project by this command : `mvn clean spring-boot:run`

## Screenshot

Login Page

![Login Page](img/login.png "Login Page")

List User Page

![List User Page](img/list.png "List User Page")

Add New User Page

![Add New User Page](img/add.png "Add New User Page")

List User2 Page

![List User2 Page](img/list2.png "List User2 Page")