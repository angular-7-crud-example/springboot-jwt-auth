-- auto-generated definition
CREATE TABLE user (
  id         INT AUTO_INCREMENT
    PRIMARY KEY,
  age        INT NULL,
  first_name VARCHAR(255) NULL,
  last_name  VARCHAR(255) NULL,
  password   VARCHAR(255) NULL,
  salary     BIGINT NULL,
  username   VARCHAR(255) NULL
)
  ENGINE = MyISAM;

