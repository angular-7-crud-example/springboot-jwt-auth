INSERT INTO user (id, age, first_name, last_name, password, salary, username)
VALUES (1, 35, 'Hatake', 'Kakashi', '$2a$10$feHoUB1Im9lHkxdw3ytlsOJU8WRT2MG6ALK/2ge28Dh/wsbcRBM2i', 270,
        'kakashi'); -- kakashi / kakashi
INSERT INTO user (id, age, first_name, last_name, password, salary, username)
VALUES (2, 17, 'Hendi', 'Santika', '$2a$10$l9qPIw3QGd8oR2RIHzaR5.VtfctaqN9W5A5FntmoYt0AqkG378rku', 150,
        'Hendi Santika'); -- hendi / hendi
INSERT INTO user (id, age, first_name, last_name, password, salary, username)
VALUES (3, 17, 'Uzumaki', 'Naruto', '$2a$10$eL4bfGKi6d8DFcH.5PzIt.YXeLljh2TT0O3/S/iTFwt4yk8Cy.b5S', 250,
        'naruto'); -- naruto / naruto
INSERT INTO user (id, age, first_name, last_name, password, salary, username)
VALUES (4, 18, 'Uchiha', 'Sasuke', '$2a$10$WALGhg1B.f3E1kIiEoSCh.565aQcYgH1YOQMF1irv7ub/4dIC6sQy', 240,
        'sasuke'); -- sasuke / sasuke
INSERT INTO user (id, age, first_name, last_name, password, salary, username)
VALUES (5, 15, 'Sakura', 'Haruno', '$2a$10$0OTOeCgtkxMrk/fH/ViNw.vtyQlbliXgYBlJnqdxm8RuXPqkbtDGm', 230,
        'sakura'); -- sakura / sakura
