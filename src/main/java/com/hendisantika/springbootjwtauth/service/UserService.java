package com.hendisantika.springbootjwtauth.service;

import com.hendisantika.springbootjwtauth.model.User;
import com.hendisantika.springbootjwtauth.model.UserDto;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-auth
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-26
 * Time: 12:40
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {

    User save(UserDto user);

    List<User> findAll();

    void delete(int id);

    User findOne(String username);

    User findById(int id);

    UserDto update(UserDto userDto);
}