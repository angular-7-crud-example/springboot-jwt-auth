package com.hendisantika.springbootjwtauth.model;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-auth
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-26
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */

@Data
public class LoginUser {
    private String username;
    private String password;
}
