package com.hendisantika.springbootjwtauth.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jwt-auth
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-26
 * Time: 12:29
 * To change this template use File | Settings | File Templates.
 */

@Data
@AllArgsConstructor
public class ApiResponse<T> {
    private int status;
    private String message;
    private Object result;

}
